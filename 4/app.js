//add eventListener
document.querySelector('#checkBtn').addEventListener('click', checkNumber);

function checkNumber(e) {
    e.preventDefault();

    //get input value
    let val = document.getElementById('valueFirst').value;

    //check entered value is blank or not
    if (val !== '') {

        //check enter value is odd or even
        let result = val % 2;

        //check reminder is 0 or not
        if (result) {
            document.getElementById('result').innerText = `${val} is a odd number`;
        } else {
            document.getElementById('result').innerText = `${val} is a even number`;
        }

    } else {
        alert('Please enter a value');
    }
}