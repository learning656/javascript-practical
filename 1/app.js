//add button listener
document.querySelector('#addBtn').addEventListener('click', calcFunction);
document.querySelector('#substractBtn').addEventListener('click', calcFunction);
document.querySelector('#multiplyBtn').addEventListener('click', calcFunction);
document.querySelector('#divisionBtn').addEventListener('click', calcFunction);
document.querySelector('#squareBtn').addEventListener('click', calcFunction);
document.querySelector('#cubeBtn').addEventListener('click', calcFunction);
document.querySelector('#remainderBtn').addEventListener('click', calcFunction);


//Calculate function
function calcFunction(e) {

    //get input box values
    let val1 = document.getElementById('valueFirst').value;
    let val2 = document.getElementById('valueSecond').value;

    //get button id
    const idName = this.id;

    //check input values filled or not
    if (val1 !== '' && val2 !== '') {

        //check case for calculation
        switch (idName) {
            case 'addBtn':
                const addTotal = +val1 + +val2;
                document.getElementById('result').value = addTotal;
                break;

            case 'substractBtn':
                const subTotal = val1 - val2;
                document.getElementById('result').value = subTotal;
                break;

            case 'multiplyBtn':
                const multipleTotal = val1 * val2;
                document.getElementById('result').value = multipleTotal;
                break;

            case 'divisionBtn':
                const divisionTotal = val1 / val2;
                document.getElementById('result').value = divisionTotal;
                break;

            case 'squareBtn':
                const squareTotal = Math.pow(val1, val2);
                document.getElementById('result').value = squareTotal;
                document.getElementById('resultText').innerHTML = `Result ${val1} exponent of ${val2}`;
                break;

            case 'cubeBtn':
                const cubeTotal1 = Math.cbrt(val1);
                const cubeTotal2 = Math.cbrt(val2);
                document.getElementById('result').value = `${cubeTotal1}, ${cubeTotal2}`;
                break;

            case 'remainderBtn':
                const remainderTotal = val1 % val2;
                document.getElementById('result').value = remainderTotal;
                break;

            default:
                document.getElementById('result').value = 'No data'
        }
    } else {
        alert('Please fill all the details')
    }

    //Clear fields
    document.getElementById('valueFirst').value = '';
    document.getElementById('valueSecond').value = '';

}