document.getElementById('checkBtn').addEventListener('click', clickFunction);
let result = 0;

function clickFunction(e) {
    e.preventDefault();

    let val = document.getElementById('valueFirst').value;
    let val2 = document.getElementById('valueSecond').value;
    var power = val;


    powerRecursion(val, val2, power)

    document.getElementById('result').innerText = result;
}


function powerRecursion(data1, data2, power) {

    if (data2 === 1) return 1

    power = power * data1;
    data2--;

    result = power;
    powerRecursion(data1, data2, power);
}