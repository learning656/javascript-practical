document.querySelector('#primeBtn').addEventListener('click', primeFunction);

document.querySelector('#armstrongBtn').addEventListener('click', armstrongFunction);

function primeFunction(e) {
    e.preventDefault();

    //get the input value
    let val = document.getElementById('valueFirst').value;

    //1 and 2 are Prime number so
    if (val === 1 || val === 2) {
        document.getElementById('result').innerHTML = 'Number is prime'
    } else {
        for (let i = 0; i < val; i++) {
            //check the module of result is 0 or not.
            var res = val % i;

            if (res === 0 && i != 1) {
                document.getElementById('result').innerHTML = 'Number is not prime'
                break;
            } else {
                document.getElementById('result').innerHTML = 'Number is prime'
            }
        }
    }
}

//armstrong number function
function armstrongFunction(e) {
    e.preventDefault();

    //get the input value
    let val1 = document.getElementById('valueFirst').value;

    if (val1) {
        //convert value into array
        let arr = val1.split("");
        let arrSum = 0;

        for (let i = 0; i < arr.length; i++) {
            arrSum = arrSum + Math.pow(arr[i], arr.length);
        }

        if (val1 == arrSum) document.getElementById('result').innerText = "Number is armstrong"
        else document.getElementById('result').innerText = "Number is not an armstrong"
    }
    else alert('Please fill the value')
}