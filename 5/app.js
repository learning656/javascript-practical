//add eventListener
document.querySelector('#checkBtn').addEventListener('click', checkChar);

function checkChar(e) {
    e.preventDefault();

    //get input value
    const val = document.getElementById('valueFirst').value;

    if (val) {
        //check whether entered value is number or not and single value
        if (isNaN(val) && val.length < 2) {

            const result = val == "A" || val == "E" || val == "I" || val == "O" || val == "U" || val == "a" || val == "e" || val == "i" || val == "o" || val == "u";

            if (result) {
                document.getElementById('result').innerText = 'Entered character is Vowel';
            } else {
                document.getElementById('result').innerText = 'Entered character is Consonant';
            }

        } else {
            document.getElementById('result').innerText = 'Please enter proper character';
        }
    } else {
        alert('Please fill the detail')
    }
}