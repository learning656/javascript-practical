//add eventListener
document.querySelector('#swapBtn').addEventListener('click', swapFunction);

//swap function
function swapFunction(e) {
    e.preventDefault();

    //get input values
    let val1 = document.getElementById('valueFirst').value;
    let val2 = document.getElementById('valueSecond').value;

    if (val1 !== '' && val2 !== '') {
        let x = val1;
        let y = val2;

        //swap values
        [x, y] = [y, x];

        document.getElementById('result').value = `Swap values: ${x}, ${y}`;

    } else {
        alert('Please fill all details')
    }
}