document.querySelector('#checkBtn').addEventListener('click', clickFunction);


function clickFunction(e) {
    e.preventDefault();

    let val = Number(document.getElementById('valueFirst').value);
    recursionFunction(val);
    document.getElementById('result').innerText = recursionFunction(val);
}

function recursionFunction(para) {
    if (para == 1)
        return 1;
    else
        return para + recursionFunction(para - 1)
}