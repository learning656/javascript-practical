document.querySelector('#checkBtn').addEventListener('click', fibonacciSum);

function fibonacciSum(e) {
    e.preventDefault();

    let val = document.getElementById('valueFirst').value;
    let a = 0;
    let b = 1;
    let sum = 0;

    if (val) {
        if (isNaN(val)) alert('Please enter a number')

        else {

            for (let i = 2; i <= val; i++) {
                sum = a + b;
                a = b;
                b = sum;
            }
            document.getElementById('result').innerHTML = b;
        }
    } else alert('Please enter a value')
}