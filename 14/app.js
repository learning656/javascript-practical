document.querySelector('#checkBtn').addEventListener('click', reverseNumber);

function reverseNumber(e) {
    e.preventDefault();

    let val = document.getElementById('valueFirst').value;

    if (val) {

        if (isNaN(val)) alert('Please enter a numeric value')
        else {
            //Convert number to string
            let str = val.toString();

            //split string
            let splitString = str.split('');

            //reverse string
            let res = splitString.reverse();

            //join string
            let joinString = res.join('');

            document.getElementById('result').innerHTML = Number(joinString);
        }

    } else alert('Please enter a value')
}