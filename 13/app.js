document.querySelector('#checkBtn').addEventListener('click', countDigit);

function countDigit(e) {
    e.preventDefault();

    let val = document.getElementById('valueFirst').value;

    if (val) {
        if (isNaN(val)) alert('Please enter integer value')
        else {
            document.getElementById('result').innerHTML = val.length;
        }

    } else alert('Please enter a value')
}