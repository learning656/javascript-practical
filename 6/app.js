document.querySelector('#checkBtn').addEventListener('click', checkMaxValue);
document.querySelector('#clearBtn').addEventListener('click', clearAll);

function checkMaxValue(e) {
    e.preventDefault();

    //Get the values
    let val1 = document.getElementById('valueFirst').value;
    let val2 = document.getElementById('valueSecond').value;
    let val3 = document.getElementById('valueThird').value;

    if (val1 || val2 || val3) {
        if (isNaN(val1) || isNaN(val2) || isNaN(val3)) {
            alert('Please enter all numeric value')
        } else {
            if (val1 > val2 && val1 > val3) {
                document.getElementById('result').innerHTML = `Number ${val1} is greater than among numbers`;
            } else if (val2 > val1 && val2 > val3) {
                document.getElementById('result').innerHTML = `Number ${val2} is greater than among numbers`;
            } else if (val3 > val1 && val3 > val2) {
                document.getElementById('result').innerHTML = `Number ${val3} is greater than among numbers`;
            } else {
                document.getElementById('result').innerHTML = `All are same`;
            }
        }
    } else {
        alert('Please enter value');
    }


}

function clearAll() {
    document.getElementById('valueFirst').value = '';
    document.getElementById('valueSecond').value = '';
    document.getElementById('valueThird').value = '';
    document.getElementById('result').innerHTML = ''
}