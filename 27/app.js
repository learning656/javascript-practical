document.querySelector('#checkBtn').addEventListener('click', clickFunction);

function clickFunction(e) {
    e.preventDefault();

    let val = document.getElementById('valueFirst').value;

    document.getElementById('result').innerText = reversString(val)

}

function reversString(para) {

    if (para === '') return ""

    return reversString(para.substr(1)) + para[0];
}