//add eventListener
document.querySelector('#calculate').addEventListener('click', calculateFunction);

//function
function calculateFunction(e) {
    e.preventDefault();

    //get value of inputs
    let val1 = document.getElementById('valueFirst').value;
    let val2 = document.getElementById('valueSecond').value;

    //check input values added or not
    if (val1 !== '' && val2 !== '') {
        //calculate quotient
        const quotientVal = Math.floor(val1 / val2);

        //calculate remainder
        const remainderVal = val1 % val2;

        //assign result to resultbox
        document.getElementById('result').value = `Quotient: ${quotientVal}, remainder: ${remainderVal}`;

    } else {
        alert('Please fill all details');
    }

    //clear fields
    document.getElementById('valueFirst').value = '';
    document.getElementById('valueSecond').value = '';
}