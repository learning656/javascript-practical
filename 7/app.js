document.querySelector('#checkBtn').addEventListener('click', sumNumber);

function sumNumber(e) {
    e.preventDefault();

    let num = document.getElementById('valueFirst').value;
    var sum = 0;

    if (num) {
        if (isNaN(num)) {
            alert('Please enter numeric value')
        } else {

            for (let i = 1; i <= num; i++) {
                sum = sum + i;
                document.getElementById('result').innerHTML = sum;
            }
        }

    } else {
        alert('Please enter value')
    }

}