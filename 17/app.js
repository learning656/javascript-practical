document.querySelector('#checkBtn').addEventListener('click', primeNumberFunction);

function primeNumberFunction(e) {
    e.preventDefault();

    const val = document.getElementById('valueFirst').value;

    if (val) {
        if (isNaN(val)) alert('Please enter integer value')

        else {
            //starting loop with 2 because every number will divide by 1 return 0
            for (let i = 2; i < val; i++) {
                //check the module of result is 0 or not.
                var res = val % i;

                if (res === 0) {
                    document.getElementById('result').innerHTML = 'Number is not prime'
                    break
                } else {
                    document.getElementById('result').innerHTML = 'Number is prime'
                }
            }

        }

    } else alert('Please fill all details')
}