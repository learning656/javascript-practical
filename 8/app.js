document.querySelector('#checkBtn').addEventListener('click', factorialSum);

function factorialSum(e) {
    e.preventDefault();

    let val = document.getElementById('valueFirst').value;
    let sum = 1;
    if (val) {
        if (isNaN(val)) alert('Please enter a number')
        else {
            for (let i = 1; i <= val; i++) {
                sum = sum * i;
            }
            document.getElementById('result').innerHTML = sum;
        }
    } else alert('Please fill value')
}