let first = "A",
    last = "Z",
    firstCode = first.charCodeAt(0),
    lastCode = last.charCodeAt(0),
    arr = [];

for (let i = firstCode; i <= lastCode; i++) {
    let str = String.fromCharCode(i);

    arr.push(str);
    document.getElementById("result").innerHTML = arr;
}
