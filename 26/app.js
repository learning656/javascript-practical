document.querySelector('#checkBtn').addEventListener('click', clickFunction);
let result = 0;

function clickFunction(e) {
    e.preventDefault();

    let val1 = document.getElementById('valueFirst').value;
    let val2 = document.getElementById('valueSecond').value;

    //calling function with arguments
    gcdFunction(val1, val2);

    //print the result
    document.getElementById('result').innerText = result;

}

function gcdFunction(para1, para2) {
    //base case [stop condition]
    if (para2 < 1) {
        return 1
    } else {

        //gcd formula
        result = para2;
        para2 = para1 % para2;
        para1 = result;

        //recursion
        gcdFunction(para1, para2);
    }
}