document.querySelector('#checkBtn').addEventListener('click', factorialFunction);

function factorialFunction(e) {
    e.preventDefault();

    let val = document.getElementById('valueFirst').value;

    if (val) {

        let arr = [];
        for (let i = 1; i <= val; i++) {
            let result = val % i;
            if (result == 0) arr.push(i);
        }
        document.getElementById('result').innerText = `Factorial of ${val} are :` + ' ' + arr;

    } else alert('Please enter a value')
}