document.querySelector('.checkBtn').addEventListener('click', btnClick);

// let clsBtn = document.getElementsByClassName('checkBtn');

// clsBtn.addEventListener('click', btnClick);

function btnClick(e) {
    e.preventDefault();

    //get input values
    let val1 = document.getElementById('valueFirst').value;
    let val2 = document.getElementById('valueSecond').value;

    primeNumberFunction(val1, val2)
}

function primeNumberFunction(param1, param2) {

    //array for collecting result of module
    let resultArr = [];
    //array for collecting values between param1 to param2
    let paramArr = [];

    if (param1 && param2) {

        //loop between 2 params values
        for (let i = Number(param1); i <= param2; i++) {
            paramArr.push(i);

            //loop for find module 0 or not
            for (let x = 2; x <= i; x++) {
                var result = i === x ? -1 : i % x;

                if (result === 0) {
                    resultArr.push(i);
                }
            }
        }

        //combine two array
        let combineArr = [...resultArr, ...paramArr];

        //array for storing unique value result from combine result and param array
        const res = [];

        //find out the unique value from combine array
        for (let i = 0; i < combineArr.length; i++) {
            //will check both position of array index at same position
            if (combineArr.lastIndexOf(combineArr[i]) === combineArr.indexOf(combineArr[i])) {
                res.push(combineArr[i]);
            };
        };

        //print the result
        document.getElementById('result').innerHTML = 'Prime numbers are: ' + res;

    } else alert('Please fill all details')

}