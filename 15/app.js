document.querySelector('#checkBtn').addEventListener('click', powerFunction);

function powerFunction(e) {
    e.preventDefault();

    const val1 = document.getElementById('valueFirst').value;
    const val2 = document.getElementById('valueSecond').value;

    if (val1 && val2) {

        if (isNaN(val1) || isNaN(val2)) {
            alert('Please enter numeric value');
        } else {
            document.getElementById('result').innerHTML = Math.pow(val1, val2);
        }

    } else {
        alert('Please fill the all details');
    }
}