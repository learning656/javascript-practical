document.querySelector('#checkBtn').addEventListener('click', palindromeFunction);

function palindromeFunction(e) {
    e.preventDefault();

    let val = document.getElementById('valueFirst').value;

    if (val) {

        if (isNaN(val)) alert('Please enter integer value');
        else {
            let numStr1 = val.toString();
            let splitString = numStr1.split('')
            let reverseString = splitString.reverse();
            let joinString = reverseString.join('');

            if (val === joinString) {
                document.getElementById('result').innerHTML = 'Palindrome'
            } else document.getElementById('result').innerHTML = 'Not Palindrome'

        }

    } else alert('Please fill the details')

}