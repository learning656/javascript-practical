document.querySelector('#checkBtn').addEventListener('click', gcdFunction);

function gcdFunction(e) {
    e.preventDefault();

    let val1 = document.getElementById('valueFirst').value;
    let val2 = document.getElementById('valueSecond').value;
    let sum;

    if (val1 && val2) {
        if (isNaN(val1) || isNaN(val2)) alert('Please add numeric values');
        else {
            for (let i = 1; i <= val2; i++) {
                sum = val2;
                val2 = val1 % val2;
                val1 = sum;
            }

            document.getElementById('result').innerHTML = `Result is: ${sum}`;
        }

    } else alert('Please fill all details')
}