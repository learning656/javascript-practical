document.querySelector('#checkBtn').addEventListener('click', primeNumberFunction);

function primeNumberFunction(e) {
    e.preventDefault();

    let val1 = document.getElementById('valueFirst').value;
    let val2 = document.getElementById('valueSecond').value;

    let resultArr = [];
    let resultArr1 = [];

    if (val1 && val2) {
        let i = Number(val1);

        //loop between 2 values
        for (i; i <= val2; i++) {
            resultArr1.push(i);

            //loop for find module 0 or not
            for (let x = 2; x <= i; x++) {
                var result = i === x ? -1 : i % x;

                if (result === 0) {
                    resultArr.push(i);
                }
            }
        }

        //combine two array
        let combineArr = resultArr.concat(resultArr1);

        const res = [];

        //find out the unique value from combine array
        for (let i = 0; i < combineArr.length; i++) {
            if (combineArr.lastIndexOf(combineArr[i]) !== combineArr.indexOf(combineArr[i])) {
                continue;
            };
            res.push(combineArr[i]);
        };

        //print the result
        document.getElementById('result').innerHTML = 'Palindrome numbers are: ' + res;

    } else alert('Please fill all details')

}