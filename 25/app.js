document.querySelector('#checkBtn').addEventListener('click', clickFunction);


function clickFunction(e) {
    e.preventDefault();

    let val = Number(document.getElementById('valueFirst').value);
    factorialFunction(val);
    document.getElementById('result').innerText = factorialFunction(val);
}

function factorialFunction(para) {
    if (para == 1)
        return 1;
    else
        return para * factorialFunction(para - 1)
}