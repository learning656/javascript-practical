document.querySelector('#checkBtn').addEventListener('click', lcmFunction);

function lcmFunction(e) {
    e.preventDefault();

    let val1 = document.getElementById('valueFirst').value;
    let val2 = document.getElementById('valueSecond').value;

    if (val1 && val2) {
        if (isNaN(val1) || isNaN(val2)) alert('Please enter numeric values')
        else {
            let largeNum = Math.max(val1, val2);
            let smallNum = Math.min(val1, val2);
            let num = largeNum;
            while (num % smallNum !== 0) {
                num += largeNum;
            }

            document.getElementById('result').innerHTML = num;
        }

    } else alert('Please fill all details')
}