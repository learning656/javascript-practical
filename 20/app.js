document.querySelector('#checkBtn').addEventListener('click', armstrongNumber);


function armstrongNumber(e) {
    e.preventDefault();
    //get the input values
    let val1 = document.getElementById('valueFirst').value;
    let val2 = document.getElementById('valueSecond').value;

    if (val1 && val2) {
        let newArr = [];

        for (let x = val1; x <= val2; x++) {
            let numberStr = x.toString().split("");

            let arrSum = 0;
            for (let i = 0; i < numberStr.length; i++) {
                arrSum = arrSum + Math.pow(numberStr[i], numberStr.length);;
            }

            if (x == arrSum) {
                newArr.push(arrSum);
            }
        }

        document.getElementById('result').innerText = newArr;

    }
    else alert('Please fill the value')


}